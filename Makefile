JVC=javac
JV=java
POOD=src/POODemo.java
PROG=POODemo
SRC=src/
BIN=bin/
GIT=git

CPR=cp -r
RM=rm -rf

all:
	$(JVC) $(POOD) -d $(BIN) -cp $(SRC)

run:
	$(JV) -cp $(BIN) $(PROG)

upload:
	echo "# This is my README" >> README.md
	$(GIT) add *
	$(GIT) commit -m "Third commit. Understand how to add now"
	$(GIT) push -u origin master

clean:
	$(RM) $(BIN)*
