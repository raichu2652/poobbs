// package POOBBS;

public class POOBoard extends POODirectory{
	private POOArticle[] Article;
	
	public POOBoard(String name) {
		super(name);
		this.Article = new POOArticle[1024];
	}
	public void add(POOArticle article) {
		this.Article[count++] = article;
	}
	public void del(int pos) {
		this.Article[pos] = null;
		for (int i = pos; i < count - 1; i++)
			this.Article[i] = this.Article[i + 1];
		count--;
	}
	public void move(int src, int dest) {
		POOArticle temp;
		temp = this.Article[dest];
		this.Article[dest] = this.Article[src];
		this.Article[src] = temp;
	}
	public int length() {
		return count;
	}
	public void show() {
		System.out.println("\n\n\n\n				/**********POOBBS**********/");
		if (count == 0)
			System.out.println("");
		else
			System.out.println("				ID		EV		Author		Title");
		for (int i = 0; i < count; i++) {
			System.out.print("			(" + i + ")");
			this.Article[i].list();
		}
		System.out.println("				/**************************/\n\n\n\n");
		System.out.print("Command: ");
	}
	public String showBoard() {
		return this.name;
	}
	public POOArticle showBoard(int boardno) {
		return this.Article[boardno];
	}
}