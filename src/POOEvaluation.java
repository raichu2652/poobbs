// package POOBBS;
import java.util.Scanner;

public class POOEvaluation {
	private int status;
	private String author;
	private String content;
	
	public POOEvaluation(int status) {
		Scanner scanner = new Scanner(System.in);
		this.status = status;
		this.author = POODemo.User.showName();
		System.out.print("Evaluation: ");
		content = scanner.nextLine();
	}
	public void show() {
		switch(status) {
			case 1:
				System.out.print("Push		"); break;
			case -1:
				System.out.print("Boo		"); break;
			case 0:
				System.out.print("--->		"); break;
		}
		System.out.println(author + "	" + content);
	}
}
