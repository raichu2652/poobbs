// package POOBBS;
import java.util.Scanner;

public class POODemo {
	public static POOUser User;
	private static POODirectory[] homeDir;
	private static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		homeDir = new POODirectory[2];
		
		System.out.print("Please enter your user name: ");
		User = new POOUser(scanner.next(), 1);
		
		homeDir[0] = new POODirectory("Homepage");
		homeDir[1] = new POODirectory("Favorite");
		
		BBS();
	}
	public static void BBS() {
		System.out.println("\n\n\n\n				/**********POOBBS**********/");
		System.out.println("					User: " + User.showName());
		System.out.println("					(H) " + homeDir[0].showDir());
		System.out.println("					(F) " + homeDir[1].showDir());
		System.out.println("					(G) GoodBye");
		System.out.println("				/**************************/\n\n\n\n");
		System.out.print("Command: ");
		
		switch(scanner.next().charAt(0)) {
			case 'H': BBS(homeDir[0]); break;
			case 'F': BBS(homeDir[1]); break;
			case 'G': return;
		}
	}
	public static void BBS(POOBoard board) {
		System.out.println("					" + board.showBoard());
		board.show();
		
		switch(scanner.next().charAt(0)) {
			case 'W':
				System.out.println("New Article");
				board.add(new POOArticle(board.length()));
				BBS(board);
				break;
			case 'D':
				System.out.print("Delete: ");
				board.del(scanner.nextInt());
				BBS(board);
				break;
			case 'M':
				System.out.print("Move: ");
				board.move(scanner.nextInt(), scanner.nextInt());
				BBS(board);
				break;
			case 'S':
				System.out.print("Show Article: ");
				BBS(board.showBoard(scanner.nextInt()));
				BBS(board);
				break;
			case 'G':
				return;
		}
	}
	public static void BBS(POODirectory directory) {
		System.out.println("					" + directory.showDir());
		directory.show();
		
		switch(scanner.next().charAt(0)) {
			case 'A':
				System.out.print("Add Directory: ");
				directory.add(new POODirectory(scanner.next()));
				BBS(directory);
				break;
			case 'B':
				System.out.print("Add Board: ");
				directory.add(new POOBoard(scanner.next()));
				BBS(directory);
				break;
			case 'S':
				directory.add_split();
				BBS(directory);
				break;
			case 'D':
				System.out.print("Delete: ");
				directory.del(scanner.nextInt());
				BBS(directory);
				break;
			case 'M':
				System.out.print("Move: ");
				directory.move(scanner.nextInt(), scanner.nextInt());
				BBS(directory);
				break;
			case 'E':
				System.out.print("Enter Board: ");
				POODirectory d = directory.showDir(scanner.nextInt());
				if (d instanceof POOBoard)
					BBS((POOBoard)d);
				else
					BBS(d);
				BBS(directory);
				break;
			case 'G':
				return;
		}
	}
	public static void BBS(POOArticle article) {
		article.show();
			
		switch(scanner.next().charAt(0)) {
			case 'P':
				System.out.println("Push this article!");
				article.push();
				break;
			case 'B':
				System.out.println("Boo this article~");
				article.boo();
				break;
			case 'E':
				article.arrow();
				break;
		}
	}
}
