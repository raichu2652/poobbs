// package POOBBS;
import java.util.Scanner;

public class POOArticle {
	private int ID;
	private String title;
	private String author;
	private String content;
	private int evcount;
	private int pushes;
	private int boos;
	private POOEvaluation[] evaluation;
	
	public POOArticle(int count) {
		Scanner scanner = new Scanner(System.in);
		this.ID = count;
		this.author = POODemo.User.showName();
		System.out.print("Title: ");
		this.title = scanner.next();
		System.out.print("Content: ");
		this.content = new String();
		String temp = scanner.next();
		while (temp.length() != 1 || temp.charAt(0) != 'Q') {
			this.content += temp + "\n";
			temp = scanner.nextLine();
		}
		evaluation = new POOEvaluation[100];
	}
	public void push() {
		evaluation[evcount++] = new POOEvaluation(1);
		pushes++;
	}
	public void boo() {
		evaluation[evcount++] = new POOEvaluation(-1);
		boos++;
	}
	public void arrow() {
		evaluation[evcount++] = new POOEvaluation(0);
	}
	public void show() {
		System.out.println("\n\n\n\n				/**********POOBBS**********/");
		System.out.println("				Title: " + title + " | Author: " + author);
		System.out.println("Content: " + content);
		System.out.println("----------------------------------------------------");
		if (evcount == 0)
			System.out.println("");
		else
			for (int i = 0; i < evcount; i++)
				evaluation[i].show();
		System.out.println("				/**************************/\n\n\n\n");
		System.out.print("Command: ");
	}
	public void list() {
		System.out.println("	" + ID + "		" + evcount + "		" + author + "		" + title);
	}
}
