// package POOBBS;

public class POODirectory {
	protected int count;
	protected String name;
	private POODirectory[] Directory;
	
	public POODirectory(String name) {
		this.name = name;
		this.Directory = new POODirectory[100];
	}
	public void add(POOBoard board) {
		this.Directory[count++] = board;
	}
	public void add(POODirectory dir) {
		this.Directory[count++] = dir;
	}
	public void add_split() {
		this.Directory[count++] = null;
	}
	public void del(int pos) {
		this.Directory[pos] = null;
		for (int i = pos; i < count - 1; i++)
			this.Directory[i] = this.Directory[i + 1];
		count--;
	}
	public void move(int src, int dest) {
		POODirectory temp;
		temp = this.Directory[dest];
		this.Directory[dest] = this.Directory[src];
		this.Directory[src] = temp;
	}
	public int length() {
		return count;
	}
	public void show() {
		System.out.println("\n\n\n\n				/**********POOBBS**********/");
		if (count == 0)
			System.out.println("");
		for (int i = 0; i < count; i++) {
			System.out.print("				(" + i + ")");
			if (this.Directory[i] == null)
				System.out.println("				----------------------------");
			else if (this.Directory[i] instanceof POOBoard)
				System.out.println("	Board: " + ((POOBoard)this.Directory[i]).showBoard());
			else
				System.out.println("	Directory: " + this.Directory[i].showDir());
		}
		System.out.println("				/**************************/\n\n\n\n");
		System.out.print("Command: ");
	}
	public String showDir() {
		return this.name;
	}
	public POODirectory showDir(int dirno) {
		return this.Directory[dirno];
	}
}